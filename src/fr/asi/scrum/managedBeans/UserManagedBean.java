package fr.asi.scrum.managedBeans;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;

import fr.asi.scrum.crud.CrudRole;
import fr.asi.scrum.crud.CrudUser;
import fr.asi.scrum.entities.Role;
import fr.asi.scrum.entities.User;

@ManagedBean(name="userMB")
@RequestScoped
public class UserManagedBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private User user;
	private User showUser;
	private List<Role> roles;
	private List<User> users;

	public UserManagedBean() {
		this.user=new User();
		this.users = CrudUser.getAll();
		this.roles = CrudRole.getAll();
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
	
	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}
	
	public User getShowUser() {
		return showUser;
	}

	public void setShowUser(User showUser) {
		this.showUser = showUser;
	}
	
	public String ajouterUser()
	{
		System.out.print("test");
		if(CrudUser.create(user))
		{
			System.out.print("test");
			return "success";
		}
		return "Erreur utilisateur na pas ete creer";
	}
	
	public String show(User user)
	{
		showUser = user;
		return "show";
	}
	
	public String delete(User user)
	{
		if (CrudUser.delete(user)) {
			return "success";
		}
		return null;
		
	}
	
	public String create()
	{
		return "create";
	}
}
