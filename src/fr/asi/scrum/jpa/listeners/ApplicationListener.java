package fr.asi.scrum.jpa.listeners;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class ApplicationListener implements ServletContextListener {

	private static EntityManagerFactory emf;
	
    public static EntityManagerFactory getEmf() { 
        return emf; 
    } 

    public static void setEmf(EntityManagerFactory emf) { 
        ApplicationListener.emf = emf; 
    } 

 
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
        if(this.emf!=null && this.emf.isOpen()) 
        { 
            this.emf.close(); 
        } 
		
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		this.emf = Persistence.createEntityManagerFactory("scrum"); 
		
	}

}
