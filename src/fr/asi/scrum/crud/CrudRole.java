package fr.asi.scrum.crud;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import fr.asi.scrum.entities.Role;
import fr.asi.scrum.jpa.listeners.ApplicationListener;

public class CrudRole extends ApplicationListener
{
	
	private static EntityManager em = getEmf().createEntityManager();
	
	public static Role getOne(int id)
	{
		 if (em.isOpen())
		 {
			 Role role = em.find(Role.class, id);
			 
			 return role;
		 }
		 return null;
	}
	
	public static List<Role> getAll()
	{
		if (em.isOpen())
		 {
			 List<Role> list= new ArrayList<Role>(em.createQuery("FROM Role", Role.class).getResultList());
			 
			 return list;
		 }
		 return null;
	}
	
	public static boolean create(String nom)
	{
		if (em.isOpen())
		{
			em.getTransaction().begin();
			
			Role role = new Role();
			role.setNom(nom);
			
			em.persist(role);
			em.getTransaction().commit();
			
			return true;
		}
		return false;
	}
	
	public static boolean update(Role role, String nom)
	{
		if (em.isOpen())
		{
			em.getTransaction().begin();

			role.setNom(nom);
			
			em.merge(role);
			em.getTransaction().commit();
			
			return true;
		}
		return false;
	}
	
	public static boolean delete(Role role)
	{
		if (em.isOpen())
		{
			em.getTransaction().begin();
			em.remove(role);
			em.getTransaction().commit();
			
			return true;
		}
		return false;
	}
	

}
