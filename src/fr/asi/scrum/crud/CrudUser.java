package fr.asi.scrum.crud;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import fr.asi.scrum.entities.Role;
import fr.asi.scrum.entities.User;
import fr.asi.scrum.jpa.listeners.ApplicationListener;

public class CrudUser extends ApplicationListener{
	
	private static EntityManager em = getEmf().createEntityManager();
	
	
	public static User getOne(int id)
	{
		if (em.isOpen())
		 {
			 User user = em.find(User.class, id);
			 
			 return user;
		 }
		 return null;
	}
	
	public static List<User> getAll()
	{
		if (em.isOpen())
		 {
			 ArrayList<User> list= new ArrayList<User>(em.createQuery("FROM User", User.class).getResultList());
			 
			 return list;
		 }
		 return null;
	}
	
	public static boolean create(User user)
	{
		if (em.isOpen())
		{
			em.getTransaction().begin();
			em.persist(user);
			em.getTransaction().commit();
			
			return true;
		}
		return false;
	}
	
	public static boolean update(User user)
	{
		if (em.isOpen())
		{
			em.getTransaction().begin();
			em.merge(user);
			em.getTransaction().commit();
			
			return true;
		}
		return false;
	}
	
	public static boolean delete (User user)
	{
		if (em.isOpen())
		{
			em.getTransaction().begin();
			em.remove(user);
			em.getTransaction().commit();
			
			return true;
		}
		return false;
	}
	
	public static User getByLogin(String login)
	{
		 if (em.isOpen())
		 {
			 User user = em.find(User.class, login);
			 
			 return user;
		 }
		 return null;
	}

}
