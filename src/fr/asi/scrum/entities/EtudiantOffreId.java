package fr.asi.scrum.entities;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class EtudiantOffreId implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private int etudiant;
	private int offre;
	
	 public int hashCode() {
		    return (int)(etudiant + offre);
		  }

		  public boolean equals(Object object) {
		    if (object instanceof EtudiantOffreId) {
		      EtudiantOffreId otherId = (EtudiantOffreId) object;
		      return (otherId.etudiant == this.etudiant) 
		              && (otherId.offre == this.offre);
		    }
		    return false;
		  }
	public int getEtudiant() {
		return etudiant;
	}
	public void setEtudiant(int etudiant) {
		this.etudiant = etudiant;
	}
	public int getOffre() {
		return offre;
	}
	public void setOffre(int offre) {
		this.offre = offre;
	}
	
	
	
}
