package fr.asi.scrum.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Diplome implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name="titre", nullable=false)
	private String titre;
	
	@ManyToMany(mappedBy="diplomes",cascade=CascadeType.ALL)
    private List<Etudiant> etudiants;
	
	@ManyToMany(mappedBy="diplomes",cascade=CascadeType.ALL)
    private List<Offre> offres;

	public Diplome() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Diplome(int id, String titre, List<Etudiant> etudiants, List<Offre> offres) {
		super();
		this.id = id;
		this.titre = titre;
		this.etudiants = etudiants;
		this.offres = offres;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public List<Etudiant> getEtudiants() {
		return etudiants;
	}

	public void setEtudiants(List<Etudiant> etudiants) {
		this.etudiants = etudiants;
	}

	public List<Offre> getOffres() {
		return offres;
	}

	public void setOffres(List<Offre> offres) {
		this.offres = offres;
	}
	
}
