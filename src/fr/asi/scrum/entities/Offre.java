package fr.asi.scrum.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Offre implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name="titre", nullable=false)
	private String titre;
	
	@Column(name="description", nullable=false)
	private String description;
	
	@Column(name="pdf", nullable=true)
	private String pdf;
	
	@ManyToMany(cascade=CascadeType.ALL)
    @JoinTable(name="diplome_offre", 
    			joinColumns=@JoinColumn(name="id_offre", referencedColumnName="id", nullable=false),
    			inverseJoinColumns = @JoinColumn(name="id_diplome", referencedColumnName="id", nullable=false))
	private List<Diplome> diplomes;
	
	@ManyToOne
	@JoinColumn(name = "entreprise_id", referencedColumnName="id")
	private Entreprise entreprise;
	
    @OneToMany(mappedBy = "etudiant")
    private List<Etudiant_offre> etudiant;

	public Offre() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Offre(int id, String titre, String description, String pdf, List<Diplome> diplomes,
			List<Etudiant> etudiants, Entreprise entreprise) {
		super();
		this.id = id;
		this.titre = titre;
		this.description = description;
		this.pdf = pdf;
		this.diplomes = diplomes;
		this.entreprise = entreprise;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPdf() {
		return pdf;
	}

	public void setPdf(String pdf) {
		this.pdf = pdf;
	}

	public List<Diplome> getDiplomes() {
		return diplomes;
	}

	public void setDiplomes(List<Diplome> diplomes) {
		this.diplomes = diplomes;
	}

	public Entreprise getEntreprise() {
		return entreprise;
	}

	public void setEntreprise(Entreprise entreprise) {
		this.entreprise = entreprise;
	}	
	
}
